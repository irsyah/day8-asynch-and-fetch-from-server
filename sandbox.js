// let data = [ 10, 5, 3, 2 ];

// data.map(function(element, index, array) {
//    console.log(element) 
// })

// data.map((el, ind, arr) => {
//     console.log(el);
// })

// data.map(iterasi)

// function iterasi(el, ind, arr) {
//     console.log(el);
//     // console.log(ind);
//     // console.log(arr);
// }




// async
// ketika kirim/terima data dari server, baca file, baca database, upload, download 
// import fs from 'fs';
const fs = require ('fs');
const fsProm = require('fs/promises');

//sync readfile
// console.log("1. Mulai baca file");
// let hasil = fs.readFileSync('./data.json', 'utf8');
// console.log(hasil);
// console.log("2. Selesai baca file");

// async readFile

function bacaFile(callback) { // parameter 1 data, parameter 2 err
    console.log(callback);
    fs.readFile('./data.json', 'utf8', function(err, data) {
        callback(data, err)
    })
}

function iterasi() {
    bacaFile(function(data, err) {
        if (err) {
            console.log(err);
        } else {
            data = JSON.parse(data);
            for (let i = 0; i < data.length; i++) {
                console.log(data[i]);
                console.log(data[i]['title']);
            }
        }
    })
}

iterasi()

console.log("==============");
// PROMISE
function bacaFilePromise() { //function bacaFilePromise me-return promise based!
    
    return new Promise((resolve, reject) => {
        fsProm.readFile('./data.json', 'utf8')
        .then(data => {
            resolve({
                data: data,
                message: 'berhasil'
            })
        })
        .catch(err => {
            console.log("masuk reject");
            reject(err)
        })
    })
}

function iterasiPromise() {
    bacaFilePromise()
    .then((data) => {
        console.log(`message: ${data.message}`)
        let hasilBaca = JSON.parse(data.data);
        hasilBaca.map(d => {
            console.log("*********");
            console.log(d);
            console.log("*********");
        })
    })
    .catch(err => {
        console.log(err);
    })
}

iterasiPromise();

// ASYNC AWAIT
async function greeting() {
    return "Hello";
}

greeting()
.then(result => {
    console.log(result);
})

async function iterasiAsynAwait() {
    try {
        let result = await bacaFilePromise();
        let data = JSON.parse(result.data);
        console.log("-------DIBACA DARI ASYNC AWAIT-------");
        data.map(d => {
            console.log(d);
        })
    } catch (err) {
        console.log(err);
    }
    
}

iterasiAsynAwait()