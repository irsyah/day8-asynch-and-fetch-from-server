import React from 'react'

function Movie(props) {
  const { movie } = props;

  return (
    <div>
        <h3>{movie.title}</h3>
        <p>{movie.description}</p>
        <img src={`/assets/${movie.poster}`} height="250px" width="250px" />
    </div>
  )
}

export default Movie