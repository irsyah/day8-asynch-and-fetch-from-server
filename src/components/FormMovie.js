import React, { useState } from 'react'

function FormMovie(props) {
    const { setMovies } = props;

    const [ newMovie, setNewMovie ] = useState({ title: '', description: '', year: '', poster: null });

    const postMovies = async () => {
        try {
            //untuk mengirimkan file multipart ke server
            const formData = new FormData();
            formData.append('img', newMovie.poster);
            formData.append('title', newMovie.title);
            formData.append('description', newMovie.description);
            formData.append('year', newMovie.year);

            const response = await fetch(`http://localhost:8080/api/movies/`, {
                method: 'POST',
                body: formData
            });


            if (response.ok) {
                let data = await response.json();
                setMovies(currState => {
                    return [ ...currState, data ];
                })
            } else {
                throw Error('something wrong')
            }
        } catch (err) {
            console.log(err);
        }
    }

    const onChangeMovie = (e) => {
        if (e.target.id === 'poster') {
            setNewMovie(currState => {
                return { ...currState, 'poster': e.target.files[0] }
            })
        } else {
            setNewMovie(currState => {
                return { ...currState, [e.target.id]: e.target.value }
            })
        }
      }

    const reset = () => {
        setNewMovie({ title: '', description: '', year: '', poster: null });
      }
    

    const save = (e) => {
        e.preventDefault();
        postMovies();
        reset();
    }

    return (
        <div>
            <form onSubmit={save}>
                <input id="title" type="text" placeholder='title' value={newMovie.title} onChange={onChangeMovie}/>
                <input id="description" type="text" placeholder='description' value={newMovie.description} onChange={onChangeMovie} />
                <input id="year" type="text" placeholder='year' value={newMovie.year} onChange={onChangeMovie}/>
                <input id="poster" type="file" accept='image/png, image/jpeg, image/jpg'  onChange={onChangeMovie}/>
                <button type='submit'>Save</button>
            </form>
        </div>
    )
}

export default FormMovie