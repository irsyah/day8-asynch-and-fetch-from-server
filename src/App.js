import React, { useState, useEffect } from "react";
import Movies from "./pages/Movies";

function App() {
  const [ users, setUsers ] = useState([]);

  const getAllUser = async () => {
    try {
      const response = await fetch(`https://jsonplaceholder.typicode.com/users`);
      if (response.status !== 200) {
        throw Error('something wrong')
      } else {
        const data = await response.json();
        setUsers(data);
      }
    } catch (err) {
      console.log(err);
    }
  }

  // contoh penggunaan fetch async await
  useEffect(() => {
    getAllUser();
  }, [])

  // CONTOH PENGGUNAAN FETCH PROMISE 
  // useEffect(() => {
  //   fetch(`https://jsonplaceholder.typicode.com/users`)
  //   .then(response => {
  //     if (response.status !== 200) {
  //       throw Error('something wrong')
  //     } else {
  //       return response.json();
  //     }
  //   })
  //   .then(data => {
  //     setUsers(data);
  //   })
  //   .catch(err => {
  //     console.log(err);
  //   })
  // }, [])

  return (
    <div>
      {/* {users.map(user => (
        <div key={user.id}>
          <h1>{user.name}</h1>
          <h3>{user.website}</h3>
        </div>
      ))} */}

      <Movies />
    </div>
  )
}


// CODINGAN MATERI COMPONENT LIFE CYCLE

// function App() {
//   const [ todo, setTodo ] = useState('');
//   const [ username, setUsername ] = useState('');
//   const [ isError, setIsError ] = useState(false);

//   useEffect(function() {
//     console.log("hanya dijalankan sekali saja saat component pertama kali dibuat");
//   }, [])

//   useEffect(() => {
//     console.log("use effect watch username berjalan!");
//     if (username.length < 5 && username.length > 0) {
//       setIsError(true);
//     } else {
//       setIsError(false);
//     }
//   }, [ username ])

//   return (
//     <div className="App">
//       <input type="text" value={todo} placeholder="todo" onChange={(e) => setTodo(e.target.value)} />
//       <input type="text" value={username} placeholder="username" onChange={(e) => setUsername(e.target.value)} />
//       { isError && <span>Username harus lebih atau sama dengan 5 karakter</span>}
//     </div>
//   );
// }

// class App extends React.Component {

//   componentDidMount() {
//     // ketika memanggil data dari server di lakukan di sini
//   }

//   componentDidUpdate() {
//     // ketika terjadi perubahan pada state
//   }

//   componentWillUnmount() {
//     // ketika component akan di remove/destroy
//   }

//   render() {
//     return (
//       <div></div>
//     )
//   }
// }

export default App;
