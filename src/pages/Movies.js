import React, { useState, useEffect} from 'react'
import FormMovie from '../components/FormMovie';
import Movie from '../components/Movie';

function Movies() {
  const [ movies, setMovies ] = useState([]);

  const fetchMovies = async () => {
    try {
        const response = await fetch(`http://localhost:8080/api/movies`);
        if (response.ok) {
            let data = await response.json();
            setMovies(data);
        } else {
            throw Error('something wrong')
        }
    } catch (err) {
        console.log(err);
    }
  }
  useEffect(() => {
    fetchMovies();
  }, [])

  return (
    <div>
       <FormMovie setMovies={setMovies}/>

        { movies.map(movie=>(
            <Movie key={movie.id} movie={movie} />
        ))}
    </div>
  )
}

export default Movies