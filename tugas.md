# Server (Java)

1. Create Model Book dengan attribute sebagai berikut:
    - id (long)
    - title (String)
    - author (String)
    - description (String)
    - cover (String)
    - price (double)

2. Buatlah REST API untuk CRUD (CREATE, READ, UPDATE, DELETE), dimana ketika function Create attribute coverBook didapatkan dari upload image

- buat private repository dengan nama book-store-server, add irsyah sebagai member dengan role developer dan beri akses sampai 1 November 2022

Batasan
- Untuk Update tidak perlu update cover!!


# Client (React JS)
- buatlah project React JS untuk book-store dengan mengambil data dari API yang kalian telah buat

- Buat form untuk add Book
- Tampilkan List Book
    - pada setiap Book terdapat tombol "Buy"
    - Jika stock 0 maka tombol "Buy" disable/hilang
- Pada saat tombol "Buy" ditekan maka panggil API untuk mengedit stock book (stock akan berkurang)

- buat private repository dengan nama book-store-client, add irsyah sebagai member dengan role developer dan beri akses sampai 1 November 2022